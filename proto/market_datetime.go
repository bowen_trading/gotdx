package proto

var (
	global_current_trading_day = "1990-12-19"
)

// CorrectTradingDay 校正当前交易日期
func CorrectTradingDay(date string) {
	global_current_trading_day = date
}

// GetTradingDay 获取当前日期
func GetTradingDay() string {
	return global_current_trading_day
}
