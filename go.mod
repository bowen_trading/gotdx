module gitee.com/quant1x/gotdx

go 1.21

require (
	gitee.com/quant1x/gox v1.11.3
	github.com/dop251/goja v0.0.0-20230828202809-3dbe69dd2b8e
	github.com/stretchr/testify v1.8.4
	golang.org/x/exp v0.0.0-20230905200255-921286631fa9
	golang.org/x/text v0.13.0
)

//replace gitee.com/quant1x/gox v1.10.6 => ../../mymmsc/gox

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dlclark/regexp2 v1.10.0 // indirect
	github.com/go-sourcemap/sourcemap v2.1.3+incompatible // indirect
	github.com/gocarina/gocsv v0.0.0-20230616125104-99d496ca653d // indirect
	github.com/google/pprof v0.0.0-20230907193218-d3ddc7976beb // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
